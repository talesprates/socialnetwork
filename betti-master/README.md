# BETTI (Better than Tidia)
Small social website with AngularJS + Sails.

## Download + Installation

`$ cd betti`

`$ npm install`


## Usage

Under betti/ you can:

`$sails lift`

open CHROME (important) and type:

`localhost:1337`

## Contact us!
Eduardo Brunaldi

Paulo G. De Mitri		paulo.mitri@usp.br

